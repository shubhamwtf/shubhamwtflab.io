---
title: Hello World
date: 2022-04-01 20:38:55
tags: Shubham, introduction
---

Shubham Verlekar
================

## Who am I? 
A flexible professional with experience developing and building IT solutions. Confident DevOps Engineer with exemplary expertise in routine application maintenance tasks, including troubleshooting and testing. Purpose-driven professional with capacity to be strong team player plus work effectively independently. Complex problem-solver with analytical and driven mindset. Dedicated to achieving demanding development objectives according to tight schedules while producing impeccable code

## Skills

**Public & Private Cloud
Technologies:**
_Microsoft Azure, Amazon Web Services (AWS),
Google Cloud Platform (GCP), OpenStack,
Pivotal Cloud Foundry (PCF)._

**Version Control Tools:**
_GIT, GITHUB, Subversion (SVN), CVS, Bitbucket._

**CI/CD**
_Jenkins, Bamboo, GitLab CI, Travis CI, Github
actions_

**Scripting:**
_Shell Scripting (Korn/Bourne/Bash), Python,
Ruby and Powershell._

**Virtualization Technologies:**
_VMware ESXi, Windows Hyper-V, Power VM,
Virtual box, KVM._

**Bug Tracking Tools:**
_Code Commit, JIRA, Bugzilla, Remedy_

**Business Planning**
_Analytics, Attention to minute detials_

**English Language**
_Native-like expertise in Grammar, Syntax and
Spellings with Wide Vocablury with exposure to
mainstream litearture._

**Practically Neccecary Technical
Skills**
_Ansible, Splunk, Service Desk, Atlassian Stack
JIRA, Confluence, Bitbucket, Bamboo/Jenkins_

```
Containerization Tools
ocker, Docker Swarm, Kubernetes, AWS ECS,
Apache Mesos, OpenShift
```
```
Configuration Management Tools
Chef, Ansible, Puppet, Salt Stack, Terraform
```
```
Build and Testing
Maven, Ant, Gradle, Selenium, JUnit, NUnit,
xUnit
```
```
Performing/Monitoring & Bug
Tracking Tools:
ELK, Nagios, CloudWatch, Azure Monitor, New
Relic, Splunk, Grafana, Prometheus,
Confluence, Jira.VMware ESXI, Vagrant, KVM,
Windows Hyper V, Power VM, vSphere 5Citrix
```
```
Web Tech and Databases:
Apache Tomcat, Nginx, WebSphere, WebLogic,
JBoss, Samba, SQL Server. Dynamo DB, MySQL,
RDBMS, NoSQL, Cassandra, PostgreSQL, Mongo
DB, Oracle DB
```
```
Mentoring and training
Topics : Linux, Containers, Development
```
```
Critical and Logical Thinking
This has to be my definition of the self.
```
```
Linux
Ninja level on RHEL, Ubuntu, Gentoo, Arch
distros.
```